$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $("#contacto").on("show.bs.modal", function (E) {
        console.log("El Modal Contacto se esta mostrando");
        $("#ContactoBtn").removeClass("btn-outline-success");
        $("#ContactoBtn").addClass("btn-primary");
        $("#ContactoBtn").prop("disabled", true);
    });
    $("#contacto").on("shown.bs.modal", function (E) {
        console.log("El Modal Contacto se Mostro");
    });
    $("#contacto").on("hide.bs.modal", function (E) {
        console.log("El Modal Contacto se Oculta");
    });
    $("#contacto").on("hidden.bs.modal", function (E) {
        console.log("El Modal Contacto se Oculto");
        $("#ContactoBtn").prop("disabled", false);
    });
});